package sse659.project2;

/* no wildcard imports */

/* IO */

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

/* JUnit */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * PNG image type input file
 *
 * @see sse659.project2.Config
 * 
 * @see <a href='http://www.jonelo.de/java/jacksum'>
 * http://www.jonelo.de/java/jacksum</>
 * 
 * @see <a href='http://www.gnupg.org/download/integrity_check.en.html'>
 * http://www.gnupg.org/download/integrity_check.en.html</>
 * 
 * @author Hatfield, Kevin
 */

public class TestPngImageFile implements Config
{
	/** png img file equivalent with known checksums */
	
	FileInputStream _pngImageFile;
	
	
	/* encap methods */
	
	
	private void set_pngImageFile(FileInputStream fileHandleIn) 
	{
		_pngImageFile = fileHandleIn;
		
	}//method

	
	private FileInputStream get_pngImageFile() 
	{
		return _pngImageFile;
		
	}//method

		
	/* functional methods */
	
	
	/**
	 * Ensure test file available, expected size
	 * 
	 * @throws Exception failure to close file
	 */
	
	@Before
	public void setUp() throws IOException
	{
		try
		{
		
			set_pngImageFile( new FileInputStream( 
					Config.testFiles.PNG_IMAGE.val() ) );

			assertNotEquals( "PNG mage test file empty", 0, get_pngImageFile().
					available() );
		
		}
		catch(FileNotFoundException fileIOerr)
		{
			System.out.print( "Could not locate file for png image test: " + 
					fileIOerr.getMessage() );
			
		}
		catch(IOException fileMissingerr)
		{
			System.out.print( "Could not access PNG image file for empty " +
					"test: " + fileMissingerr.getMessage() );
			
		}
		catch(Exception fileOthererr)
		{
			System.out.print( "Unexpected error empty test: " + 
					fileOthererr.getMessage() );
			
		}		
		finally
		{
			if( get_pngImageFile() != null ) get_pngImageFile().close();
		
		}//try
		
	}//method

	
	/**
	 * verify expected PNG image file checksum values for several algorithms
	 */
	
	@Test
	public void testMain() 
	{
		try
		{

			for(String algorithmStr : Config.pngImageChecksums.TYPES.
					supported() )
			{
				String args[] = {"-a", algorithmStr, "-f", 
						Config.testFiles.PNG_IMAGE.val() };

				//need capture std out

				/* http://stackoverflow.com/questions/2169330/
				 * java-junit-capture-the-standard-input-output-for-use-in-a
				 * -unit-test
				 */

				final ByteArrayOutputStream outputStream = 
						new ByteArrayOutputStream();

				System.setOut(new PrintStream(outputStream));

				jonelo.jacksum.cli.Jacksum.main(args);

				final String stdOut = outputStream.toString().split("\\s")[0];
				
				assertEquals("Unexpected " + algorithmStr + " for PNG image " +
						" file", Config.pngImageChecksums.TYPES.
						fromStr(algorithmStr).val(), stdOut);

				assertNotEquals("Unexpected null output " + algorithmStr + 
						" for PNG image file", null, stdOut);
				
			}//for

		}
		catch(Exception unexpErr)
		{
			System.out.print("Unexpected exception, test PNG image file: " + 
					unexpErr.getMessage());
			
		}//try
		
	}//method	
	
}//class
