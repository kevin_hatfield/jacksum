package sse659.project2;

/* no wildcard imports */

/* IO */

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/* JUnit */

import java.io.PrintStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

/**
 * Single char input files <ul><li>null char<li>empty char</ul>
 *
 * @see sse659.project2.Config
 * 
 * @see <a href='http://www.jonelo.de/java/jacksum'>
 * http://www.jonelo.de/java/jacksum</>
 * 
 * @see <a href='http://www.gnupg.org/download/integrity_check.en.html'>
 * http://www.gnupg.org/download/integrity_check.en.html</>
 * 
 * @author Hatfield, Kevin
 */

public class TestReservedCharFile implements Config
{
	FileInputStream _nullCharFile;
	FileInputStream _emptyCharFile;
	
	/* encap methods */
	
	
	private FileInputStream get_emptyCharFile() { return _emptyCharFile; }
	
	
	private FileInputStream get_nullCharFile()  { return _nullCharFile;  }
	
	
	private void set_emptyCharFile(FileInputStream fileIn) 
	{ 
		_emptyCharFile = fileIn; 
	
	}//method
	
	
	private void set_nullCharFile(FileInputStream fileIn)  
	{ 
		_nullCharFile = fileIn;  
	
	}//method

	
	/* JUnit test */
	
	
	/** ensure files available, expected contents */
	
	@Before
	public void setUp() throws IOException
	{
		try
		{
			/* open OK */
			
			set_emptyCharFile( new FileInputStream(Config.testFiles.
					EMPTY_CHAR_FILE.val()) );
			
			set_nullCharFile( new FileInputStream(Config.testFiles.
					NULL_CHAR_FILE.val()) );
			
			
			/* size OK */
			
			if( get_emptyCharFile().available() != 1 )
				fail("Empty Char File corrupt: single character expected");

			if( get_nullCharFile().available() != 1 )
				fail("Null Char File corrupt: single character expected");

			get_emptyCharFile().close();
			get_nullCharFile().close();

			
			/* contents OK */
			
			assertEquals("Corrupt empty char test file", Config.EMPTY_CHAR,
			
					UtilitiesForTests.getSingleCharFileContents(
							Config.testFiles.EMPTY_CHAR_FILE.val() ) );
			
			assertEquals("Corrupt null char test file", Config.NULL_CHAR,
					
					UtilitiesForTests.getSingleCharFileContents(
							Config.testFiles.NULL_CHAR_FILE.val() ) );
			
		}
		catch(FileNotFoundException fileIOerr)
		{
			System.out.print( "Could not locate char file for test: " + 
					fileIOerr.getMessage() );
			
		}
		catch(IOException fileMissingerr)
		{
			System.out.print( "Could not access char file for test " +
					fileMissingerr.getMessage() );
			
		}
		catch(Exception fileOthererr)
		{
			System.out.print( "Unexpected error char file test: " + 
					fileOthererr.getMessage() );
			
		}		
		finally
		{
			if(get_emptyCharFile() != null) get_emptyCharFile().close();
			if(get_nullCharFile()  != null) get_nullCharFile().close();
			
		}//try
		
	}//method
	
	
	/** verify expected checksum values for several algorithms */
	
	@Test
	public void testMain() 
	{
		try
		{

			for(String algorithmStr : Config.charChecksums.EMPTY.hashData()
					.keySet() )
			{

				/* build parameter strings */
				
				
				String emptyCharArgs[] = {"-a", algorithmStr, "-f", 
						Config.testFiles.EMPTY_CHAR_FILE.val() };

				String nullCharArgs[] = {"-a", algorithmStr, "-f", 
						Config.testFiles.NULL_CHAR_FILE.val() };
				
				
				//need capture std out

				/* http://stackoverflow.com/questions/2169330/
				 * java-junit-capture-the-standard-input-output-for-use-in-a
				 * -unit-test
				 */

				
				/* run for empty char file */
				
				
				final ByteArrayOutputStream outputStreamEmptyChar = 
						new ByteArrayOutputStream();

				System.setOut(new PrintStream(outputStreamEmptyChar));

				jonelo.jacksum.cli.Jacksum.main(emptyCharArgs);

				
				/* run for null char file */
				
				
				final ByteArrayOutputStream outputStreamNullChar = 
						new ByteArrayOutputStream();

				System.setOut(new PrintStream(outputStreamNullChar));

				jonelo.jacksum.cli.Jacksum.main(nullCharArgs);
				
				
				/* verify results */

				
				final String stdOutEmptyChar = outputStreamEmptyChar.
						toString().split("\\s")[0];

				final String stdOutNullChar = outputStreamNullChar.
						toString().split("\\s")[0];

				
				assertEquals("Unexpected " + algorithmStr + " empty char file",
						Config.charChecksums.EMPTY.hashData().
						get(algorithmStr), stdOutEmptyChar);

				
				assertEquals("Unexpected " + algorithmStr + " null char file",
						Config.charChecksums.NULL.hashData().
						get(algorithmStr), stdOutNullChar);
				
				
				assertNotEquals("Empty Char file equivalency Null Char file",
						stdOutEmptyChar, stdOutNullChar);
				
			}//for

		}
		catch(Exception unexpErr)
		{
			System.out.print("Unexpected exception, test empty char file: " + 
					unexpErr.getMessage());
			
		}//try
		
		
	}//method	
	
}//class
