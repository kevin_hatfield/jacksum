package sse659.project2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import jonelo.sugar.util.BubbleBabble;

import org.junit.Before;
import org.junit.Test;

/**
 * JUnit test, unit level<br><br>
 * 
 * Test encode method: BubbleBabble byte[] => String encoding
 * 
 * @author Hatfield, Kevin
 */

public class TestBubbleBabble 
{
	/**
	 * JUnit setup()<br><br>
	 * 
	 * Parse xml log data from file containing known test results captured
	 * from Jacksum BubbleBabble encode method<br><br>
	 * 
	 * Byte data is wrapped in xml 
	 * <input-6>�E�{6���▒������</input-6><output-6>xucog-hymel-retez-vofir-
	 * fokir-bocym-kuhuz-digun-nuxyx</output-6> 
	 * 
	 * <ul>
	 * <li>Read BubbleBabble encode method raw byte[] input
	 * <li>Read BubbleBabble encode method String encoded output
	 * <li>Parse, validate, store in Collection for tests against Jacksum 
	 * BubbleBabble encode method via testEncode() method
	 * </ul>
	 * 
	 * @see Config
	 */
	
	@Before
	public void setUp()
	{
		try
		{
			/* retrieve known input, results */

			final byte[] results = Files.readAllBytes((Paths.get(Config.
					testFiles.LOG_BB_ENCODE_BYTES.val())));
			
			final ByteBuffer resultsBuf = ByteBuffer.wrap(results).
					asReadOnlyBuffer();

			BubbleBabbleResultsRecord.fromBuffer(resultsBuf);
			
			assertNotEquals("Zero results parsed from xml log", new Integer(0), 
					BubbleBabbleResultsRecord.count());
							
		}
		catch(IOException fileErr)
		{
			System.out.println("Unable read results file: " + Config.testFiles.
					LOG_BB_ENCODE_BYTES.val() + " " + fileErr.getMessage());
		
		}//try
		
	}//method
	
	
	/**
	 * JUnit test<br><br>
	 * 
	 * Iterate known input, compare input and results from log xml file to live
	 * Jacksum method
	 * 
	 * @see BubbleBabble
	 */
	
	@Test
	public final void testEncode() 
	{
		runEncode();
		
		//runEncode("summary");
		//runEncode("verbose");
		
	}//method
	
	
	/**
	 * JUnit test<br><br> 
	 * 
	 * Empty byte value, compare Jacksum against refactored version of 
	 * BubbleBabble
	 */
	
	@Test
	public final void testEmpty()
	{
		final byte[] _empty = new String("").getBytes();
		
		assertEquals("BubbleBabble byte String encode varies, empty test: ", 
				BubbleBabble.encode(_empty),
				BubbleBabbleRefactored.encode(_empty));

	}//method
	

	/**
	 * JUnit test: Exception test of NullPointer<br><br> 
	 * 
	 * Null value, seek Exception Jacksum and Refactored versions of 
	 * BubbleBabble
	 */
	
	@Test
	public final void testNull()
	{
		/* test Jacksum version */
		
		try
		{
			BubbleBabble.encode(null);
		
			fail("Null test: Jacksum ver BubbleBabble encode fail to cause " + 
					"NullPointerException");
		}
		catch(NullPointerException expectedErr)
		{
			;
		}
		catch(Exception blindsidedErr)			
		{
			fail("Null test: Jacksum ver BubbleBabble encode fail to cause " + 
					"NullPointerException, made other error: " + 
					blindsidedErr.getMessage());
			
		}//try
		
		/* test refactored version */
		
		try
		{
			BubbleBabbleRefactored.encode(null);
		
			fail("Null test: Refactored ver BubbleBabble encode fail to " + 
					"cause NullPointerException");
		}
		catch(NullPointerException expectedErr)
		{
			;
		}
		catch(Exception blindsidedErr)			
		{
			fail("Null test: BubbleBabbleRefactored ver BubbleBabble encode " + 
					"fail to cause NullPointerException, made other error: " + 
					blindsidedErr.getMessage());
			
		}//try
		
	}//method

	
	/**
	 * Execute JUnit test
	 *  
	 * @param args String Optional "verbose"
	 */
	
	private void runEncode(String... args) 
	{	
		List<String> parms = Arrays.asList(args);
		
		Integer safety = 0;
		
		while( !BubbleBabbleResultsRecord.isEOF() )
		{
			if(safety++ > 100000) fail("Looping BubbleBabble test results " +
					"verification");

			BubbleBabbleResultsRecord recIn = 
					BubbleBabbleResultsRecord.getRecord();
			
			assertNotEquals("BubbleBabble byte String encode empty, test #: " + 
					recIn.key(), BubbleBabble.encode(recIn.raw()),
					"");

			/* Test of Jacksum version of BubbleBabble encode */
			
			assertEquals("BubbleBabble byte String encode varies, test #: " + 
					recIn.key(), BubbleBabble.encode(recIn.raw()),
					recIn.encoded());

			/* Test of Refactored version of BubbleBabble encode */
			
			assertEquals("Refactored BubbleBabble byte String encode varies," +
					"test #: " + recIn.key(), 
					BubbleBabbleRefactored.encode(recIn.raw()),
					recIn.encoded());
			
			if(parms.contains("verbose"))
				System.out.println("BubbleBabble test verified, test #: " + 
						recIn.key());
						
		}//while

		if(parms.contains("verbose") || parms.contains("summary"))
			System.out.println("BubbleBabble tests verified, #: " + 
					BubbleBabbleResultsRecord.starts() + 	
					" through " + 
					BubbleBabbleResultsRecord.ends() );
		
	}//method
	
}//class
