package sse659.project2;

/* no wildcard imports */

/* IO */

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/* JUnit */

import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.ListIterator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

/**
 * Test combined algorithm arguments: sha1+md5 as supported by Jacksum
 *
 * @see sse659.project2.Config
 * 
 * @see <a href='http://www.jonelo.de/java/jacksum'>
 * http://www.jonelo.de/java/jacksum</>
 * 
 * @see <a href='http://www.gnupg.org/download/integrity_check.en.html'>
 * http://www.gnupg.org/download/integrity_check.en.html</>
 * 
 * @author Hatfield, Kevin
 */

public class TestCombinedAlgorithms implements Config
{
	FileInputStream _resultsFile = null;

	ArrayList<String> _knownResults;
	
	/* encap methods */
	
	
	private FileInputStream get_resultsFile() { return _resultsFile; }
	
	
	private void set_resultsFile(FileInputStream fileIn)
	{
		_resultsFile = fileIn;
		
	}//method
	
	
	private ArrayList<String> get_knownResults(){ return _knownResults; }
	
	
	private void set_knownResults(ArrayList<String> arrListIn)
	{
		_knownResults = arrListIn;
		
	}//method

	
	/* JUnit test */
	
	
	/** ensure files available, expected contents */
	
	@Before
	public void setUp() throws IOException
	{
		BufferedReader buffRdrResults = null;
		
		try
		{		
			set_resultsFile(new FileInputStream(Config.testFiles.
					RESULTS_COMBINED_BATTERY.val()));
		
			final Integer resultsFileSize = get_resultsFile().available();
			
			if( resultsFileSize < 1 )
			{
				throw new Exception("File Empty: " + Config.testFiles.
						RESULTS_COMBINED_BATTERY.val());
			}//if

			get_resultsFile().close();
			
			
			buffRdrResults = new BufferedReader( new FileReader( Config.
					testFiles.RESULTS_COMBINED_BATTERY.val()));
			
			CharBuffer charBuff = CharBuffer.allocate(resultsFileSize);
			
			buffRdrResults.read(charBuff);
			
			buffRdrResults.close();

			//verify number of lines

			charBuff.rewind();
			
			Integer lineCt = 0;
			
			String recIn = "";
			
			ArrayList<String> expectedTestResults = new ArrayList<String>();
			
			for(Integer pos=0; pos<charBuff.length(); pos++)
			{
				Character charIn = charBuff.get(pos);
				
				if( charIn.compareTo((char)10) == 0 )
				{ 
					lineCt++;
					
					expectedTestResults.add(recIn);
					
					recIn = "";
					
				}
				else
				{
					recIn += charIn;
					
				}//if
				
			}//for
			
			assertEquals("Unexpected number of combined algorithm tests", 
					(Integer) (Config.COMBINED_ALGORITHM_TESTS * 
							UtilitiesForTests.get_testFiles().size()), lineCt);

			set_knownResults(expectedTestResults);
			
			assertEquals("Error parsing, incorrect count of combined " +
					"algorithm tests", lineCt, 
					(Integer) get_knownResults().size());

		}
		catch(FileNotFoundException fileIOerr)
		{
			System.out.print( "Could not locate file for test: " + 
					fileIOerr.getMessage() );
			
		}
		catch(IOException fileMissingerr)
		{
			System.out.print( "Could not access file for test " +
					fileMissingerr.getMessage() );
			
		}
		catch(Exception fileOthererr)
		{
			System.out.print( "Unexpected setUp() for Combined Test " + 
					"Algorithms: " + fileOthererr.getMessage() );
			
		}		
		finally
		{			
			if( get_resultsFile() != null) get_resultsFile().close();
			
			if( buffRdrResults    != null) buffRdrResults.close();
			
		}//try
		
		
	}//method


	/** 
	 * Verify expected checksum values<br><br>
	 * 
	 * For each known combined algorithm argument: '-a sha512+whirlpool0+md5' 
	 */
	
	@Test
	public void testMain() 
	{
		/* iterate through known results, repeat each test and verify */

		ListIterator<String> knownResultsIter = get_knownResults().
				listIterator(0);
		
		while( knownResultsIter.hasNext() )
		{
			String resultExpected[] = knownResultsIter.next().split(","); 
			
			assertEquals( "Corrupt combined algorithms results in file", 
					4, resultExpected.length );
			
			Double testNumber = Double.valueOf(resultExpected[0]);
			
			if( testNumber < 1 || testNumber > Config.
					COMBINED_ALGORITHM_TESTS * 
					UtilitiesForTests.get_testFiles().size() )
			{
				fail("Test result numbering in file invalid: " + testNumber);
				
			}//if
			
			String algorithmsCombined = resultExpected[1];
			String filenameUsed       = resultExpected[2];
			String checksumResult     = resultExpected[3];
			
			assertEquals("Combined algorithm result varies for test: " + 
					testNumber, checksumResult, UtilitiesForTests.
					runJacksum(filenameUsed, algorithmsCombined) );
			
		}//while
		
	}//method
	
}//class
