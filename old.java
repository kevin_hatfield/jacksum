package sse659.project2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ListIterator;

import jonelo.sugar.util.BubbleBabble;

import org.junit.Before;
import org.junit.Test;

/**
 * JUnit test, unit level<br><br>
 * 
 * Test encode method: BubbleBabble byte[] => String encoding
 * 
 * @author Hatfield, Kevin
 */

public class TestBubbleBabble 
{
        final ArrayList<ResultsRecord> _resultsLst = 
                        new ArrayList<ResultsRecord>();
        
        @Before
        public void setUp()
        {
                try
                {
                        /* retrieve known input, results */

                        final byte[] results = Files.readAllBytes((Paths.get(Config.
                                        testFiles.LOG_BB_ENCODE_BYTES.val())));

                        final Integer testStartNumber = 2;
                        final Integer testEndNumber = 448;
                        
                        /*
                         * Byte data is wrapped in xml 
                         * <input-6>ï¿½Eï¿½{6ï¿½ï¿½ï¿½â–’ï¿½ï¿½ï¿½ï¿½ï¿½ï¿½</input-6><output-6>xucog-hymel-retez-vo
                         * fir-fokir-bocym-kuhuz-digun-nuxyx</output-6> 
                         */
                        
                        final ByteBuffer resultsBuf = ByteBuffer.wrap(results).
                                        asReadOnlyBuffer();

                        resultsBuf.rewind();
                                                
                        for(Integer testNum=testStartNumber; testNum <= testEndNumber; 
                                        testNum++)
                        {
                                ResultsRecord resultsParsed = 
                                                resultsFromBuffer(testNum, resultsBuf);
                                
                                assertNotEquals("Failed to locate results for test: " + 
                                                testNum, new Integer(0), resultsParsed.key());
                                
                                _resultsLst.add(resultsParsed);
                                
                        }//for                                          
                        
                }
                catch(IOException fileErr)
                {
                        fail("Unable read results file: " + Config.testFiles.
                                        LOG_BB_ENCODE_BYTES.val() + " " + fileErr.getMessage());
                
                }//try
                
        }//method

        
        private ResultsRecord resultsFromBuffer(final Integer recNum, final ByteBuffer 
                        resultsBuf)
        {
                byte[] outputEndTag   = ("</output-" + recNum).getBytes();

                Integer safety = 0;

                ArrayList<Byte> inputData = new ArrayList<Byte>();
                String outputData = "";
                
                String findings="";
                
                while(resultsBuf.remaining() > outputEndTag.length+3)
                {
                        if(safety++ > 900000) fail("Looping reading results from " +
                                        "buffer");
                        
                        byte[] inputStartTag = ("<input-" + recNum + ">").getBytes();
                        byte[] inputEndTag   = ("</input-"+ recNum + ">").getBytes();

                        byte[] outputStartTag = ("<output-" + recNum+">").getBytes();
                               outputEndTag   = ("</output-"+ recNum+">").getBytes();
                        
                        byte[] dataIn = new byte[inputStartTag.length]; 
                        
                        Integer startInputPos = resultsBuf.position();
                        
                        resultsBuf.get(dataIn);
                        
                        boolean inputStartTagFound = true;

                        if( ! Arrays.equals(dataIn, inputStartTag) ) 
                                inputStartTagFound = false;
                        
                        if( inputStartTagFound )
                        {
                                //System.out.println("Found: "+new String(inputStartTag));
                                
                                findings += recNum + " input:" + (startInputPos + 
                                                inputStartTag.length + 1);
                                
                                while(resultsBuf.remaining() > outputEndTag.length+3)
                                {
                                        if(safety++ > 900000) fail("Looping reading results " + 
                                                        "from buffer, inner loop seeking: " + 
                                                        new String(inputEndTag));
                                        
                                        Integer endInputPos = resultsBuf.position();
                                        
                                        dataIn = new byte[inputEndTag.length]; 
                                        
                                        resultsBuf.get(dataIn);
                                        
                                        boolean inputEndTagFound = true;

                                        if( ! Arrays.equals(dataIn, inputEndTag) ) 
                                                inputEndTagFound = false;
                                
                                        if( inputEndTagFound )
                                        {
                                                findings += "-" + endInputPos + " ";
                                                
                                                while(resultsBuf.remaining() > outputEndTag.
                                                                length+3)
                                                {
                                                        if(safety++ > 900000) fail("Looping reading " +
                                                                        "results from buffer, inner loop " +
                                                                        "seeking: " + 
                                                                        new String(outputStartTag));

                                                        Integer startOutputPos = resultsBuf.position();
                                                        
                                                        dataIn = new byte[outputStartTag.length]; 
                                                        
                                                        resultsBuf.get(dataIn);
                                                        
                                                        boolean outputStartTagFound = true;

                                                        if( ! Arrays.equals(dataIn, outputStartTag) ) 
                                                                outputStartTagFound = false;
                                                
                                                        if( outputStartTagFound )
                                                        {
                                                                
                                                                findings+= " output:" + 
                                                                (startOutputPos+outputStartTag.length+1);
                                                                
                                                                while(resultsBuf.remaining() >= outputEndTag.
                                                                                length)
                                                                {
                                                                        
                                                                        if(safety++ > 900000) fail("Looping reading " +
                                                                                        "results from buffer, inner loop " +
                                                                                        "seeking: " + 
                                                                                        new String(outputEndTag));

                                                                        Integer endOutputPos = resultsBuf.position();
                                                                        
                                                                        dataIn = new byte[outputEndTag.length]; 
                                                                        
                                                                        resultsBuf.get(dataIn);
                                                                        
                                                                        boolean outputEndTagFound = true;

                                                                        if( ! Arrays.equals(dataIn, outputEndTag) ) 
                                                                                outputEndTagFound = false;
                                                                
                                                                        if( outputEndTagFound )
                                                                        {
                                                                                resultsBuf.rewind();
                                                                                
                                                                                startInputPos += inputStartTag.length;
                                                                                
                                                                                resultsBuf.position(startInputPos);
                                                                                
                                                                                byte[] inputDataBytes = new byte[endInputPos-startInputPos];
                                                                                
                                                                                resultsBuf.get(inputDataBytes, 0, endInputPos-startInputPos);

                                                                                startOutputPos += outputStartTag.length;
                                                                                
                                                                                resultsBuf.position(startOutputPos);

                                                                                byte[] outputDataBytes = new byte[endOutputPos-startOutputPos];
                                                                                
                                                                                resultsBuf.get(outputDataBytes, 0, endOutputPos-startOutputPos);
                                                                                
                                                                                String outputDataStr = new String(outputDataBytes);
                                                                                
                                                                                return new ResultsRecord(recNum,inputDataBytes,outputDataStr);
                                                                                
                                                                                //findings+="-" + endOutputPos + " ";
                                                                                
                                                                                //return findings;
                                                                                
                                                                        }else{

                                                                                resultsBuf.position(resultsBuf.position()-dataIn.length+1);

                                                                        }//if
                                                                        
                                                                }//while
                                                        
                                                        }else{

                                                                resultsBuf.position(resultsBuf.position()-dataIn.length+1);
                                                                
                                                        }//if
                                                        
                                                }//while
                                                
                                        }else{
                                                
                                                resultsBuf.position(resultsBuf.position()-dataIn.length+1);
                                                
                                        }//if
                                        
                                }//while
                                
                        }
                        else
                        {
                                resultsBuf.position(resultsBuf.position()-dataIn.length+1);
                                
                                //System.out.println(resultsBuf.position());
                                
                        }//if

                }//while

                return new ResultsRecord(0,new byte[0],"not found");
                
        }//method
        
        
        
        @Test
        public final void testEncode() 
        {
                //iterate known input, compare results
                
                ListIterator<ResultsRecord> resultsIter = _resultsLst.
                                listIterator();
                
                while( resultsIter.hasNext() )
                {
                        ResultsRecord recIn = resultsIter.next();
                        
                        //System.out.print(recIn.key());
                        //System.out.print(" ");
                        //System.out.println(recIn._dataEncoded);
                        
                        assertEquals("Bubblebabble byte String encode varies, test " + 
                                        recIn.key(), BubbleBabble.encode(recIn.get_dataRaw()),
                                        recIn.get_dataEncoded());
                        
                }//while

        }//method


        /**
         * BubbleBabble encode method input and output
         * 
         * <ul>
         * <li>Parsed from xml as found in file
         * </ul>
         */
        
        protected class ResultsRecord //implements Ordering 
        {
                private Integer _key;
                private byte[] _dataRaw;
                private String _dataEncoded;

                /**
                 * BubbleBabble encode method input and output
                 * 
                 * @param keyIn Integer record number as in file xml tags
                 * 
                 * @param dataIn Byte[] capture from BubbleBabble encode(byte[] input)
                 * 
                 * @param
                 */
                
                ResultsRecord(final Integer keyIn, final byte[] dataInput, final 
                                String dataOutput)
                {
                        set_key(keyIn);
                        set_dataRaw(dataInput);
                        set_dataEncoded(dataOutput);
                        
                }//constructor
                
                
                /* encap methods */
                
                private void set_key(final Integer keyIn){ _key = keyIn; }
                
                
                private Integer get_key(){ return _key; }

                
                private void set_dataRaw(final byte[] dataIn)
                { 
                        _dataRaw = dataIn;
                        
                }//method

                
                private byte[] get_dataRaw(){ return _dataRaw; }

                
                private void set_dataEncoded(final String dataOutput)
                { 
                        _dataEncoded = dataOutput;
                                        
                }//method
                
                
                private String get_dataEncoded(){ return _dataEncoded; }
                
                
                /* functional methods */

                
                byte[] raw(){ return get_dataRaw(); }

                
                Integer key(){ return get_key(); }
                
                
                String encoded(){ return get_dataEncoded(); }
                
        }//inner class
        
        
}//class