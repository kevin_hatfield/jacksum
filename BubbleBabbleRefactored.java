package sse659.project2;

import java.util.Arrays;
import java.util.List;

/**
 * Refactored version of BubbleBabble class using TDD, JUnit testing
 * 
 * @see BubbleBabbleResultsRecord
 * @see TestBubbleBabble
 * @see jonelo.sugar.util.BubbleBabble
 * 
 * @author Hatfield, Kevin
 */

public class BubbleBabbleRefactored 
{
	@SuppressWarnings("static-access")
	public static String encode(byte[] raw) 
	{
		if( Data.load(raw) instanceof NullData )  
			throw new NullPointerException("Invalid byte[] argument value " +
					"was null");
		
		final Data dataIn = Data.load(raw); 
		
		int seed = 1;
		
		/* Replace temp with query, Folwer p. 120 */
		
		StringBuffer retval = new StringBuffer(dataIn.rounds()*6);
		
		retval.append('x');

		for (int i=0; i < dataIn.rounds(); i++) 
		{
			int idx0, idx1, idx2, idx3, idx4;

			if ( ( i + 1 < dataIn.rounds() ) || ((dataIn.len() % 2) != 0) ) 
			{
				idx0 = ((( (dataIn.at(2 * i) & 0xff) >>> 6) & 3) + seed) 
						% 6;
				
				idx1 =   ( (dataIn.at(2 * i) & 0xff) >>> 2) & 15;
				idx2 =  (( (dataIn.at(2 * i) & 0xff) & 3) + (seed / 6)) 
						% 6;

				retval.append(AlphaCharacters.at(idx0,'v'));
				
				retval.append(AlphaCharacters.at(idx1,'c'));
				
				retval.append(AlphaCharacters.at(idx2,'v'));

				if( i+1 < dataIn.rounds() ) 
				{
					idx3 =   ( (dataIn.at(2 * i + 1) & 0xff) >>> 4) & 15;
					idx4 =     (dataIn.at(2 * i + 1) & 0xff) & 15;

					retval.append(AlphaCharacters.at(idx3,'c'));
					
					retval.append('-');
					
					retval.append(AlphaCharacters.at(idx4,'c'));
					
					seed = ((seed * 5) +
							(((dataIn.at(2 * i) & 0xff) * 7) +
									(dataIn.at(2 * i + 1) & 0xff))) % 36;
				}//if
			} 
			else 
			{
				idx0 = seed % 6;
				idx1 = 16;
				idx2 = seed / 6;
				
				retval.append(AlphaCharacters.at(idx0,'v'));
				
				retval.append(AlphaCharacters.at(idx1,'c'));
				
				retval.append(AlphaCharacters.at(idx2,'v'));
				
			}//if
			
		}//for

		retval.append('x');
		return retval.toString();
		
	}//method

	
	/**
	 * Replace array with object, Fowler p. 186<br><br>
	 * 
	 * Encapsulate collection, Fowler p. 208<br><br>
	 * 
	 * Vowels collection, lowercase<br>
	 * Selected consonants collection
	 */
	
	private static class AlphaCharacters
	{
		/** lowercase vowels used by BubbleBabble encode byte into String */
		
		private final static List<Character> _vowelChars = Arrays.
				asList(new Character[] { 'a', 'e', 'i', 'o', 'u', 'y' });
		
		
		/** 
		 * lowercase consonants used by BubbleBabble encode byte into 
		 * String 
		 */
		
		private final static List<Character> _consonantsChars = Arrays.
				asList(new Character[] { 'b', 'c', 'd', 'f', 'g', 'h', 'k', 
						'l', 'm', 'n', 'p', 'r', 's', 't', 'v', 'z', 'x' });
		
		/* encap methods */
		
		/**
		 * Retrieve single lowercase Character
		 * 
		 * @param position alpha order of vowels or consonant letters
		 * 
		 * @param alphaType Character 'v' for vowel, 'c' for consonant is 
		 * default
		 * 
		 * @return Character lowercase at given position
		 */
		
		protected static Character at(Integer position, Character alphaType)
		{
			final List<Character> letters = alphaType.compareTo('v') == 0 ? 
					_vowelChars : _consonantsChars; 
			
			if( position >= letters.size() )
			{
				throw new ArrayIndexOutOfBoundsException("Invalid: " + 
						position + " Maximum: " + ( letters.size() - 1 ) );
				
			}//if

			if( position < 0 )
			{
				throw new ArrayIndexOutOfBoundsException("Invalid: " + 
						position + " Minimum is zero" );
				
			}//if

			return letters.get(position);
			
		}//method

		
		protected static Character at(Integer position)
		{
			return at(position,'c');
			
		}//method
		
	}//inner class

	
	/** 
	 * Replace data value with object Fowler p. 175<br><br>
	 * 
	 * Replace array with object Fowler p. 186<br><br>
	 * 
	 * Class replaces "byte[] raw" parameter
	 */
	
	private static class Data
	{
		private static byte[] _rawBytes; 
		
		/* Constructor required by null version of object */
		
		Data()
		{ 
			super();
			
		}//constructor

		
		/* factory methods */
		
		/**
		 * Replace constructor with factory method Fowler p. 304<br><br>
		 * 
		 * load: factory method, return NullData when appropriate
		 * 
		 * @param rawBytesIn byte[] encode input
		 * 
		 * @return Data or NullData instance
		 */
		
		static Data load(byte[] rawBytesIn)
		{
			if(rawBytesIn != null)
			{
				set_rawBytes(rawBytesIn);
				return new Data();
			   
			}
			else
			{
				return new NullData();
				   
			}//if
			
		}//method
		
		
		/* encap methods */
		
		private static void set_rawBytes(byte[] rawBytesIn)
		{ 
			_rawBytes = rawBytesIn;
			
		}//method
		
		private static byte[] get_rawBytes(){ return _rawBytes; }

		
		/* functional methods */

		
		/**
		 * Encapsulate array, Fowler p. 215<br><br>
		 * 
		 * Encapsulate downcast, (int), Fowler p. 308<br><br>
		 * 
		 * Retrieve single byte data from encode byte[] input
		 * 
		 * @param position Integer
		 * 
		 * @return byte at specified position
		 * 
		 * @throws ArrayIndexOutOfBoundsException byte[] length less than 
		 * position
		 */
		
		protected static int at(Integer position) 
				throws ArrayIndexOutOfBoundsException
		{
			if( position >= len() )
			{
				throw new ArrayIndexOutOfBoundsException("Invalid: " + 
						position + " Maximum: " + ( len() - 1 ) );
				
			}//if
			
			if( position < 0 )
			{
				throw new ArrayIndexOutOfBoundsException("Invalid: " + 
						position + " Minimum is zero" );
				
			}//if

			return (int) get_rawBytes()[position];
			
		}//method
		
		
		/**
		 * Extract method Fowler p. 110<br><br>
		 * 
		 * @return Integer half length of encode input byte[] + 1
		 */
		
		protected static Integer rounds(){ return (len() / 2) + 1; }

		
		/** @return Integer length of byte[] encode input */
		
		protected static Integer len() { return get_rawBytes().length; }
		
		
		/* predicate methods */
		
		
		/** Always false for Data instance */
		
		protected static boolean isNull() { return false; }

		
		/** Verify zero length of byte[] encode input */
		
		protected static boolean isEmpty() 
		{ 
			return get_rawBytes().length < 1 ? true : false;
			
		}//method

		
	}//inner class
	
	
	/**
	 * Introduce Null Object Folwer p.260
	 *
	 * Empty byte[] supplied to encode method
	 */
	
	final static private class NullData extends Data 
	{
		NullData()
		{
			super();
			
		}//constructor
		
		
		/* predicate methods */
		
		//Note: Could not Overwrite the static inner class methods
		
		protected static boolean isNull(){ return true; }

		protected static boolean isEmpty(){ return true; }
		
		protected static Integer len() { return -1; }
		
	}//inner class
	
}//class
