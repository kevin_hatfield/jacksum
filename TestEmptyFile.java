package sse659.project2;

/* no wildcard imports */

/* IO */

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

/* JUnit */

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Empty input file
 *
 * @see sse659.project2.Config
 * 
 * @see <a href='http://www.jonelo.de/java/jacksum'>
 * http://www.jonelo.de/java/jacksum</>
 * 
 * @see <a href='http://www.gnupg.org/download/integrity_check.en.html'>
 * http://www.gnupg.org/download/integrity_check.en.html</>
 * 
 * @author Hatfield, Kevin
 */

public class TestEmptyFile implements Config
{
	/** zero byte file equivalent to /dev/null */
	
	FileInputStream _emptyFile;
	FileInputStream _nonEmptyFile;	
	
	
	/* encap methods */
	
	
	private void set_emptyFile(FileInputStream fileHandleIn) 
	{
		_emptyFile = fileHandleIn;
		
	}//method

	
	private FileInputStream get_emptyFile() 
	{
		return _emptyFile;
		
	}//method

	
	private void set_nonEmptyFile(FileInputStream fileHandleIn) 
	{
		_nonEmptyFile = fileHandleIn;
		
	}//method

	
	private FileInputStream get_nonEmptyFile() 
	{
		return _nonEmptyFile;
		
	}//method

	
	/* functional methods */
	
	
	/**
	 * Ensure test file available, expected size
	 * 
	 * @throws Exception failure to close file
	 */
	
	@Before
	public void setUp() throws IOException
	{
		try
		{
		
			set_emptyFile( new FileInputStream( 
					Config.testFiles.EMPTY.val() ) );

			set_nonEmptyFile( new FileInputStream( 
				Config.testFiles.PNG_IMAGE.val() ) );

			assertEquals( "File not empty", 0, get_emptyFile().available() );

			assertNotEquals( "Non-empty file equivalancy", 
				get_nonEmptyFile().available(),
				get_emptyFile().available() );
		
		}
		catch(FileNotFoundException fileMissingerr)
		{
			System.out.print( "Could not locate file for empty test: " + 
					fileMissingerr.getMessage() );
			
		}
		catch(IOException fileIOerr)
		{
			System.out.print( "Could not access file for empty test: " + 
					fileIOerr.getMessage() );
			
		}
		catch(Exception fileOthererr)
		{
			System.out.print( "Unexpected error empty test: " + 
					fileOthererr.getMessage() );
			
		}		
		finally
		{
			if( get_emptyFile() != null ) get_emptyFile().close();
		
			if( get_nonEmptyFile() != null ) get_nonEmptyFile().close();
		
		}//try
		
	}//method

	
	/**
	 * verify expected null checksum values for several algorithms
	 */
	
	@Test
	public void testMain() 
	{
		try
		{

			for(String algorithmStr : Config.nullChecksums.TYPES.supported() )
			{
				String args[] = {"-a", algorithmStr, "-f", 
						Config.testFiles.EMPTY.val() };

				//need capture std out

				/* http://stackoverflow.com/questions/2169330/
				 * java-junit-capture-the-standard-input-output-for-use-in-a
				 * -unit-test
				 */

				final ByteArrayOutputStream outputStream = 
						new ByteArrayOutputStream();

				System.setOut(new PrintStream(outputStream));

				jonelo.jacksum.cli.Jacksum.main(args);

				final String stdOut = outputStream.toString().split("\\s")[0];
				
				assertEquals("Unexpected " + algorithmStr + " for empty file",
						Config.nullChecksums.TYPES.fromStr(algorithmStr).val(), 
						stdOut);

				assertNotEquals("Unexpected null output " + algorithmStr + 
						" for empty file", null, stdOut);
				
			}//for

		}
		catch(Exception unexpErr)
		{
			System.out.print("Unexpected exception, test of empty file: " + 
					unexpErr.getMessage());
			
		}//try
		
	}//method	
	
}//class
