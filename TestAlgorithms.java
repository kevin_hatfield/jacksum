package sse659.project2;

/* no wildcard imports */

/* IO */

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/* JUnit */

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Before;
import org.junit.Test;

/**
 * Test each algorithm supported by Jacksum
 *
 * @see sse659.project2.Config
 * 
 * @see <a href='http://www.jonelo.de/java/jacksum'>
 * http://www.jonelo.de/java/jacksum</>
 * 
 * @see <a href='http://www.gnupg.org/download/integrity_check.en.html'>
 * http://www.gnupg.org/download/integrity_check.en.html</>
 * 
 * @author Hatfield, Kevin
 */

public class TestAlgorithms implements Config
{
	FileInputStream _nullCharFile;
	FileInputStream _emptyCharFile;
	FileInputStream _emptyFile;
	FileInputStream _pngImgFile;
	FileInputStream _resultsFile;
	
	List<String> _knownResults;
	
	/* encap methods */
	
	
	private FileInputStream get_emptyCharFile() { return _emptyCharFile; }
	
	
	private FileInputStream get_nullCharFile()  { return _nullCharFile;  }
	
	
	private FileInputStream get_emptyFile() { return _emptyFile; }

	
	private FileInputStream get_pngImgFile() { return _pngImgFile; }
	
	
	private FileInputStream get_resultsFile() { return _resultsFile; }
	
	
	private void set_emptyCharFile(FileInputStream fileIn) 
	{ 
		_emptyCharFile = fileIn; 
	
	}//method
	
	
	private void set_nullCharFile(FileInputStream fileIn)  
	{ 
		_nullCharFile = fileIn;  
	
	}//method

	
	private void set_emptyFile(FileInputStream fileIn)  
	{ 
		_emptyFile = fileIn;  
	
	}//method

	
	private void set_pngImgFile(FileInputStream fileIn)  
	{ 
		_pngImgFile = fileIn;  
	
	}//method

	
	private void set_resultsFile(FileInputStream fileIn)
	{
		_resultsFile = fileIn;
		
	}//method
	
	
	private void set_knownResults(List<String> listIn)
	{
		_knownResults = listIn;
		
	}//method

	
	private List<String> get_knownResults()
	{
		return _knownResults;
		
	}//method

	
	/**
	 * Search known Jacksum results for a given algorithm and test file input 
	 * 
	 * @param algorithmIn String name of hash md5 sha1
	 * @param testFileIn String path and file name
	 * @return String checksum expected 
	 */
	
	@SuppressWarnings("unused")
	private String referenceChecksum(String algorithmIn, String testFileIn)
	{
		Iterator<String> knownResultsIterator = get_knownResults().
				listIterator(0);

		String resultLine = "";

		//remove any path
		testFileIn = testFileIn.replace( Config.PATH_TESTFILES + 
				System.getProperty("file.separator"), "");
		
		while( knownResultsIterator.hasNext() )
		{
			resultLine = knownResultsIterator.next();
			
			if(resultLine.contains(","+algorithmIn+","+testFileIn))
			{
				System.out.println(algorithmIn+" : "+resultLine.split(",")[1]);
				
				return resultLine.split(",")[1];
				
			}//if
			
		}//while
		
		return "";
		
	}//method
	
	
	/* JUnit test */
	
	
	/** ensure files available, expected contents */
	
	@Before
	public void setUp() throws IOException
	{
		try
		{
			/* open OK */
			
			set_emptyCharFile( new FileInputStream(Config.testFiles.
					EMPTY_CHAR_FILE.val()) );
			
			set_nullCharFile( new FileInputStream(Config.testFiles.
					NULL_CHAR_FILE.val()) );

			set_emptyFile( new FileInputStream(Config.testFiles.
					EMPTY.val()) );
					
			set_pngImgFile( new FileInputStream(Config.testFiles.
					PNG_IMAGE.val()) );
			
			set_resultsFile( new FileInputStream(Config.testFiles.
					RESULTS_BATTERY.val()) );
			
			/* size OK */
			
			assertEquals("Empty Char File corrupt: single character expected", 
					Config.testFiles.EMPTY_CHAR_FILE.size(), 
					(Integer) get_emptyCharFile().available());

			assertEquals("Null Char File corrupt: single character expected",
					Config.testFiles.NULL_CHAR_FILE.size(),
					(Integer) get_nullCharFile().available());

			assertEquals("Empty file expected",
					Config.testFiles.EMPTY.size(),
					(Integer) get_emptyFile().available());
					
			assertEquals("PNG image file corrupt: size unexpected",
					Config.testFiles.PNG_IMAGE.size(),
					(Integer) get_pngImgFile().available());

			assertEquals("Battery test results file corrupt: size unexpected",
					Config.testFiles.RESULTS_BATTERY.size(),
					(Integer) get_resultsFile().available());
			
			/* close OK */
			
			get_emptyCharFile().close();
			get_nullCharFile() .close();
			get_emptyFile()    .close();
			get_pngImgFile()   .close();
			get_resultsFile()  .close();
			
			/* contents OK */
			
			//verify single char test files
			
			assertEquals("Corrupt empty char test file", Config.EMPTY_CHAR,
			
					UtilitiesForTests.getSingleCharFileContents(
							Config.testFiles.EMPTY_CHAR_FILE.val() ) );
			
			assertEquals("Corrupt null char test file", Config.NULL_CHAR,
					
					UtilitiesForTests.getSingleCharFileContents(
							Config.testFiles.NULL_CHAR_FILE.val() ) );
		
			//verify png image test file
			
			BufferedImage pngImage = ImageIO.read(new File(Config.testFiles.
					PNG_IMAGE.val()));
			
			assertEquals("PNG test file image: height unexpected",
					Config.testFiles.PNG_IMAGE.numAttr().get("imageHeight"),
					(Integer) pngImage.getHeight());

			assertEquals("PNG test file image: width unexpected",
					Config.testFiles.PNG_IMAGE.numAttr().get("imageWidth"),
					(Integer) pngImage.getWidth());
			
		}
		catch(FileNotFoundException fileIOerr)
		{
			System.out.print( "Could not locate file for test: " + 
					fileIOerr.getMessage() );
			
		}
		catch(IOException fileMissingerr)
		{
			System.out.print( "Could not access file for test " +
					fileMissingerr.getMessage() );
			
		}
		catch(Exception fileOthererr)
		{
			System.out.print( "Unexpected setUp() for Test Algorithms: " + 
					fileOthererr.getMessage() );
			
		}		
		finally
		{
			if(get_emptyCharFile() != null) get_emptyCharFile().close();
			if(get_nullCharFile()  != null) get_nullCharFile().close();
			
			if(get_emptyFile()  != null) get_emptyFile().close();
			if(get_pngImgFile() != null) get_pngImgFile().close();
			
			if(get_resultsFile() != null) get_resultsFile().close();
			
		}//try
		
	}//method
	
	
	/** verify expected checksum values for every algorithm */
	
	@Test
	public void testMain() 
	{
		try
		{
			//create file of test results for every algorithm
			
			UtilitiesForTests.createBatteryOfResults(Config.testFiles.
					RESULTS_TEMP.val());

			//compare each result individually

			List<String> tmpResults =
					
					Files.readAllLines( Paths.get(Config.testFiles.
					RESULTS_TEMP.val()), Charset.defaultCharset() );
			
			set_knownResults( Files.readAllLines( Paths.get(Config.testFiles.
					RESULTS_BATTERY.val()),	Charset.defaultCharset() ) );
			
			Collections.sort(tmpResults);
			Collections.sort(get_knownResults());

			assertEquals("Wrong number test results",get_knownResults().size(),
					tmpResults.size());
			
			assertNotEquals("Duplicate or mismatched test results",
					get_knownResults().get(0),tmpResults.get(1));

			Iterator<String> knownResultsIterator = get_knownResults().
					listIterator(0);
			Iterator<String> tmpResultsIterator   = tmpResults.
					listIterator(0);

			while(knownResultsIterator.hasNext() && 
					tmpResultsIterator.hasNext())
			{
				assertEquals("Algorithm result varies",
						knownResultsIterator.next(),tmpResultsIterator.next());
				
			}//while
						
			Files.delete(Paths.get(Config.testFiles.RESULTS_TEMP.val()));
			
		}
		catch(Exception unexpErr)
		{
			System.out.print("Unexpected exception, test all algorithms: " +  
					unexpErr.getMessage());

		}//try
		
	}//method	

	
	
}//class
