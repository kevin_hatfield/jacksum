package sse659.project2;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Test several algorithms supported by JackSum
 *
 * @see <a href='http://www.jonelo.de/java/jacksum'>
 * http://www.jonelo.de/java/jacksum</>
 * 
 * @see <a href='http://www.gnupg.org/download/integrity_check.en.html'>
 * http://www.gnupg.org/download/integrity_check.en.html</>
 * 
 * @author Hatfield, Kevin
 */

@RunWith(Suite.class)
@Suite.SuiteClasses
(
 {
	 TestEmptyFile.class,
	 TestPngImageFile.class,
	 TestReservedCharFile.class,
	 TestAlgorithms.class,
	 TestCombinedAlgorithms.class
 }
)

class SuiteOfFunctionalityTests{} //class