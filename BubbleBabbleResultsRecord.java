package sse659.project2;

import static org.junit.Assert.fail;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * BubbleBabble encode method input and output
 * 
 * <ul>
 * <li>encode method input: Raw byte data
 * <li>encode method output: String encoded version of byte data 
 * <li>BubbleBabbleResultsRecord: input, output parsed from xml
 * </ul>
 * 
 * <br>Byte data is wrapped in xml 
 * <input-6>�E�{6���▒������</input-6><output-6>xucog-hymel-retez-vofir-fokir
 * -bocym-kuhuz-digun-nuxyx</output-6> 
 * 
 * <br><br>UtilitiesForTests logBytes method captures data used here
 * 
 * @see UtilitiesForTests
 * 
 * @see BubbleBabble
 */

class BubbleBabbleResultsRecord
{
	
	/** ResultsRecord instances parsed from xml log of test results */
	
	private static ArrayList<BubbleBabbleResultsRecord> _entries = 
			new ArrayList<BubbleBabbleResultsRecord>();
	
	
	/** 
	 * Record number in xml tags of results log, capture of BubbleBabble encode 
	 * method input and output
	 */
	
	private static Integer _recordNumber = 0;
	
	
	/** @return BubbleBabbleResultsRecord at next position */
	
	protected static BubbleBabbleResultsRecord getRecord()
	{
		return _entries.get(_recordNumber++);
		
	}//method
	
	
	/** Reposition at start of results */
	
	protected static void rew()
	{
		_recordNumber = 0;
		
	}//method

	
	/** @return True is at end of results collection */
	
	protected static Boolean isEOF()
	{
		return _recordNumber >= count() ? true : false;
		
	}//method

	
	/** 
	 * ByteBuffer provided to Results class containing byte[] of xml log 
	 * data 
	 */
	
	private static ByteBuffer _buff;
	
	
	/** 
	 * Integer record number located in complete records: xml opening and 
	 * closing tags 
	 */
	
	private Integer _key;
	
	
	/** byte[] BubbleBabble encode method input capture as in xml log */
	
	private byte[] _dataRaw;

	
	/** String BubbleBabble encode method output capture as in xml log */

	private String _dataEncoded;
	
	
	/**
	 * BubbleBabble encode method input and output
	 * 
	 * @param keyIn Integer record number as in file xml tags
	 * 
	 * @param dataInput Byte[] capture from BubbleBabble encode(byte[] input)
	 * 
	 * @param dataOutput String BubbleBabble encode output
	 */

	BubbleBabbleResultsRecord(final Integer keyIn, final byte[] dataInput, 
			final String dataOutput)
	{
		init();
		
		set_key(keyIn);
		set_dataRaw(dataInput);
		set_dataEncoded(dataOutput);

	}//constructor

	
	/** 
	 * Create BubbleBabbleResultsRecord containing empty instance variables
	 */

	BubbleBabbleResultsRecord()
	{
		init();
		
	}//constructor

	
	/** 
	 * Factory return instance
	 * 
	 * @param keyIn Integer record number as in file xml tags
	 * 
	 * @param dataInput Byte[] capture from BubbleBabble encode(byte[] input)
	 * 
	 * @param dataOutput String BubbleBabble encode output
	 *  
	 * @return ResultsRecord 
	 */

	protected BubbleBabbleResultsRecord spawn(final Integer keyIn, final byte[] 
			dataInput, final String dataOutput)
	{
		return new BubbleBabbleResultsRecord(keyIn,dataInput,dataOutput);

	}//method
	
	
	/** Clear instance variables, set static defaults */
	
	private void init()
	{
		set_key(0);
		set_dataRaw(new byte[0]);
		set_dataEncoded("");
	
		if(_entries == null) 
			_entries = new ArrayList<BubbleBabbleResultsRecord>();
		
	}//method

	
	/* encap: instance methods */
	
	
	/** @param _key Integer record number in xml results log */

	private void set_key(final Integer keyIn){ _key = keyIn; }


	/** @return _key Integer record number in xml results log */
	
	private Integer get_key(){ return _key; }

	
	/** 
	 * @param dataIn byte[] byte array from xml results log, encode method 
	 * input capture from Jacksum
	 */

	private void set_dataRaw(final byte[] dataIn)
	{ 
		_dataRaw = dataIn;

	}//method


	/** 
	 * @return _dataRaw byte[] byte array from xml results log, encode method 
	 * input capture from Jacksum
	 */
	
	private byte[] get_dataRaw(){ return _dataRaw; }


	/**
	 * @param dataOutput String BubbleBabble encode method output as in xml
	 * results log
	 */
	
	private void set_dataEncoded(final String dataOutput)
	{ 
		_dataEncoded = dataOutput;

	}//method

	
	/**
	 * @return _dataEncoded String BubbleBabble encode method output as in xml
	 * results log
	 */
	
	private String get_dataEncoded(){ return _dataEncoded; }

	
	/* encap: collection, static methods */


	/**
	 * Static byte buffer instance
	 *  
	 * @param buffIn ByteBuffer xml log as raw bytes , mixed String and byte 
	 * data 
	 */
	
	private static void set_byteBuffer(ByteBuffer buffIn)
	{ 
		_buff = buffIn; 
	
	}//method

	
	/** 
	 * BubbleBabble captured input, output from results log file or other as
	 * byte buffer provided
	 * 
	 * @return ByteBuffer xml log as raw bytes , mixed String and byte data 
	 */
	
	private static ByteBuffer get_byteBuffer(){ return _buff; }
	

	/**
	 * Append BubbleBabbleResultsRecord to static arraylist collection
	 * 
	 * @param resultsRecIn BubbleBabbleResultsRecord parsed input, output data
	 * for encode method of BubbleBabble class
	 * 
	 * @see BubbleBabble
	 */
	
	private static void addEntry(BubbleBabbleResultsRecord resultsRecIn)
	{ 
		_entries.add(resultsRecIn);
		
	}//method

					
	/**
	 * Read only List collection derived from ArrayList object
	 * 
	 * @return List BubbleBabbleResultsRecord instances parsed from xml, from 
	 * byte buffer
	 */
	
	protected static List<BubbleBabbleResultsRecord> get_entries()
	{ 
		return Collections.unmodifiableList(_entries);
		
	}//method

	
	/** 
	 * Number of complete results records: with opening and closing xml 
	 * tags 
	 */
	
	protected static Integer count(){ return get_entries().size(); }
				
	
	/* functional methods */

	
	/** BubbleBabble byte[] input capture as within xml tags in log */

	byte[] raw(){ return get_dataRaw(); }

	
	/** 
	 * BubbleBabble encode method test results record number in xml tags 
	 * found in log 
	 */

	Integer key(){ return get_key(); }

	
	/** BubbleBabble encode output: byte[] input as String output */

	String encoded(){ return get_dataEncoded(); }

	
	/** 
	 * First ResultsRecord record number found in xml results data log
	 * <br><br>
	 * Note: record number from first complete opening and closing tag
	 */
	
	protected static Integer starts()
	{ 
		if(count() < 1) return 0;
		
		return get_entries().get(0).key();
		
	}//method


	/** 
	 * Last ResultsRecord record number found in xml results data log
	 * <br><br>
	 * Note: record number from last complete record: opening and closing 
	 * tag found
	 */
	
	protected static Integer ends()
	{ 
		if(count() < 1) return 0;
		
		return get_entries().get(count()-1).key();
		
	}//method

	
	/**
	 * Parse results from xml test results log into ResultsRecord entries
	 * 
	 * @param buffIn ByteBuffer byte[] wrapped as read only buffer, bytes
	 * from xml test results log 
	 * 
	 * @return Count Integer tally complete records found: records with 
	 * opening and closing xml tags
	 */
	
	protected static Integer fromBuffer(ByteBuffer buffIn)
	{
		Integer recNum = 0;
		
		set_byteBuffer(buffIn);
		
		get_byteBuffer().rewind();
		
		Integer safety = 0;
		
		while(recNum < Config.testFiles.LOG_BB_ENCODE_BYTES.recCount())
		{
			recNum++;
			
			byte[] inputStartTag  = ("<input-"  + recNum + ">").getBytes();
			byte[] inputEndTag    = ("</input-" + recNum + ">").getBytes();

			byte[] outputStartTag = ("<output-"  + recNum +">").getBytes();
			byte[] outputEndTag   = ("</output-" + recNum     ).getBytes();

			/* locate data positions in buffer without xml */
		
			Integer startInputPos  = xmlTagLocation(inputStartTag); 
					
			Integer endInputPos    = xmlTagLocation(inputEndTag);                

			Integer startOutputPos = xmlTagLocation(outputStartTag);
			
			Integer endOutputPos   = xmlTagLocation(outputEndTag);                

			//if xml opening and closing tags all located, for input and output
			
			if( startInputPos    > 0 && 
				  endInputPos    > 0 &&
				  
				  startOutputPos > 0 && 
				  endOutputPos   > 0 )
			{
			
				startInputPos  += inputStartTag.length;

				startOutputPos += outputStartTag.length;                

				/* copy data from buffer */

				get_byteBuffer().rewind();

				get_byteBuffer().position(startInputPos);

				byte[] inputDataBytes = 
						new byte[endInputPos-startInputPos];

				get_byteBuffer().get(inputDataBytes, 0, 
						endInputPos-startInputPos);

				get_byteBuffer().position(startOutputPos);

				byte[] outputDataBytes = 
						new byte[endOutputPos-startOutputPos];

				get_byteBuffer().get(outputDataBytes, 0, endOutputPos-
						startOutputPos);

				String outputDataStr = new String(outputDataBytes);

				/* return record instance */

				addEntry( new BubbleBabbleResultsRecord(recNum, 
						inputDataBytes, outputDataStr) );

			}//if record complete, xml closing and opening tags located
			
			if(safety++ > 1000000) fail("Looping parsing BubbleBabble " +
					" encode method results");
			
		}//while
		
		return count();
		
	}//method
	
	
	/**
	 * Search ByteBuffer for an XML tag, locating start position of xml tag
	 * 
	 * <br><br>Note: buffer is static variable, search resumes previous 
	 * position for successive calls 
	 * 
	 * @param tag byte[] containing XML tag String as bytes
	 * 
	 * @return Integer start position, 0 if not found
	 */
	
	private static Integer xmlTagLocation(byte[] tag)
	{
		Integer safety = 0;
		
		while(get_byteBuffer().remaining() > tag.length)
		{
			if(safety++ > 900000) fail("Looping seeking " + tag);

			byte[] dataIn = new byte[tag.length]; 
			
			Integer startPos = get_byteBuffer().position();
			
			get_byteBuffer().get(dataIn);
						
			if( Arrays.equals(dataIn, tag) ) return startPos;
			
			get_byteBuffer().position(get_byteBuffer().position() - 
					tag.length + 1);
		
		}//while
		
		get_byteBuffer().rewind();
		
		return 0;
		
	}//method
	
}//class

