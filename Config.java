package sse659.project2;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Constants for JUnit tests of JackSum
 * 
 * @see <a href='http://www.jonelo.de/java/jacksum'>
 * http://www.jonelo.de/java/jacksum</>
 * 
 * @see <a href='http://www.gnupg.org/download/integrity_check.en.html'>
 * http://www.gnupg.org/download/integrity_check.en.html</>
 * 
 * @author Hatfield, Kevin
 */

interface Config 
{
	/** Relative path, directory containing test files for input, output */
	
	final static String PATH_TESTFILES = "JUnit_test_files";
	
	/** Number of tests using combined algorithm arguments: sha1+md5 */
	
	final static Integer COMBINED_ALGORITHM_TESTS = 50;
	
	/** Reserved Character for Null */
	
	final static Character NULL_CHAR = new Character('\u0000');

	/** Reserved Character for Empty */

	final static Character EMPTY_CHAR = new Character('\uFFFD');

	
	/** Test filenames and sizes */
	
	static enum testFiles
	{
		/** Test file which is completely empty, no bytes */
		
		EMPTY ("empty_file.dat", 0),
		
		/** Test file containing png image of known size, dimensions */

		PNG_IMAGE ("image.png", 37600, 445, 837),

		/** Test file containing single Character reserved as null */

		NULL_CHAR_FILE ("null_char.dat", 1),
		
		/** Test file containing single Character reserved as empty */
		
		EMPTY_CHAR_FILE ("empty_char.dat", 1),
		
		/** Results from every algorithm */
		
		RESULTS_BATTERY ("results.dat", 15537),
		
		/** Results temporary file output. Used for verifying tests */
		
		RESULTS_TEMP ("results.tmp", 0),
		
		/** Combined algorithms arguments tests results: '-a sha1+md5' */
		
		RESULTS_COMBINED_BATTERY ("results_combined.dat", -1),	//size unused
		
		/** 
		 * Log file of byte data captured from BubbleBabble method: encode
		 * <br><br>
		 * 3rd attribute: Maximum number of xml records provided 
		 */
		
		LOG_BB_ENCODE_BYTES ("log_bubblebabble_encode.dat", -1, 500);
		
		private String _filename;
		private Integer _filesize;
		
		private Integer _records;

		private final Map<String,Integer> _extendedNumericAttributes = 
				new HashMap<String,Integer>();
		
		
		testFiles(String fileNameStr, Integer sizeIn)
		{
			set_fileName( PATH_TESTFILES + 
					System.getProperty("file.separator") + fileNameStr );
			
			set_filesize(sizeIn);
			
		}//constructor
		
		
		testFiles(String fileNameStr, Integer sizeIn, Integer heightIn, 
				Integer widthIn)
		{
			set_fileName( PATH_TESTFILES + 
					System.getProperty("file.separator") + fileNameStr );
			
			set_filesize(sizeIn);

			_extendedNumericAttributes.put("imageHeight", heightIn);
			_extendedNumericAttributes.put("imageWidth" , widthIn );
			
		}//constructor
		
		
		testFiles(String fileNameStr, Integer sizeIn, Integer recCountIn)
		{
			set_fileName( PATH_TESTFILES + 
					System.getProperty("file.separator") + fileNameStr );

			set_filesize(sizeIn);

			set_recCount(recCountIn);
			
		}//constructor
		
		
		/* encapsulation methods */

		
		private void set_recCount(Integer recCountIn)
		{
			_records = recCountIn;
			
		}//method
		
		
		private void set_filesize(Integer sizeIn)
		{
			_filesize = sizeIn;
			
		}//method

		
		private Integer get_filesize()
		{
			return _filesize;
			
		}//method
		
		
		private void set_fileName(String fileNameStr)
		{
			_filename = fileNameStr;
			
		}//method


		private String get_fileName()
		{
			return(_filename);
			
		}//method
		
		
		public Map<String,Integer> numAttr()
		{
			return Collections.unmodifiableMap(_extendedNumericAttributes);
			
		}//method
		
		
		/* functional methods */
		
		/** @return String test filename */
		
		public String val()
		{
			return get_fileName();
			
		}//method
		
		
		/** @return Integer file size in bytes */
		
		public Integer size()
		{
			return get_filesize();
			
		}//method
		
		
		/** @return Integer number of records */
		
		public Integer recCount()
		{
			return _records;
			
		}//method
		
	}//enum

	
	/**
	 * Known checksums for /dev/null or empty file 
	 */
	
	static enum nullChecksums
	 { 
		/** Completely empty file: md5 checksum */
		
		MD5_NULL ( "d41d8cd98f00b204e9800998ecf8427e" ),

		/** Completely empty file: sha1 checksum */

		SHA1_NULL ( "da39a3ee5e6b4b0d3255bfef95601890afd80709" ),
		
		/** Completely empty file: sha224 checksum */

		SHA224_NULL ( "d14a028c2a3a2bc9476102bb288234c415a2b0" +
				"1f828ea62ac5b3e42f" ),
				
		/** Completely empty file: sha256 checksum */

		SHA256_NULL ( "e3b0c44298fc1c149afbf4c8996fb92427ae41" +
				"e4649b934ca495991b7852b855" ),
				
		/** Completely empty file: sha512 checksum */

		SHA512_NULL ( "cf83e1357eefb8bdf1542850d66d8007d620e4" + 
				"050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0f"  +
				"f8318d2877eec2f63b931bd47417a81a538327af927da3e" ),

		/** Completely empty file: checksums known for tests */

		TYPES ( "md5","sha1","sha224","sha256","sha512" );
		
		private String _checksumStr;
		
		private String[] _types;
		
		
		nullChecksums(String checksumIn)
		{
			set_checksumStr(checksumIn);
			
		}//constructor
		
		
		nullChecksums(String... typesIn)
		{
			set_types(typesIn);
			
		}//constructor

		
		/* encap methods */
		
		private void set_types(String[] typesIn)
		{
			_types = typesIn;
			
		}//method
		

		private String[] get_types()
		{
			return _types;
			
		}//method

		
		private void set_checksumStr(String checksumIn)
		{
			_checksumStr = checksumIn;
			
		}//method

		
		private String get_checksumStr()
		{
			return _checksumStr;
			
		}//method

		
		/* functional methods */
		
		protected String val()
		{
			return get_checksumStr();
			
		}//method
		

		protected String[] supported()
		{
			return get_types();
			
		}//method
		
		
		/**
		 * Convert String to Enum
		 *  
		 * @return Enum nullchecksums
		 * 
		 * @throws Exception if String not match known Enum type
		 */
		
		protected nullChecksums fromStr(String typeInStr) 
				throws Exception
		{
			switch (typeInStr)
			{
				case "md5":
					return nullChecksums.MD5_NULL;

				case "sha1":
					return nullChecksums.SHA1_NULL;

				case "sha224":
					return nullChecksums.SHA224_NULL;

				case "sha256":
					return nullChecksums.SHA256_NULL;

				case "sha512":
					return nullChecksums.SHA512_NULL;
					
				default:
					throw new Exception("Unrecognized nullchecksum type: " + 
							typeInStr);
			}//switch
			
		}//method
	
	 }//enum	


	/**
	 * Known checksums for png image test file 
	 */
	
	static enum pngImageChecksums
	 { 
		/** Png image file: md5 checksum */
		
		MD5_NULL ( "c745a37b36fe8fb31ab0878614f29199" ),

		/** Png image file: sha1 checksum */

		SHA1_NULL ( "2a83ff7baf83e05890e3b4dd962c09d5ba1a4d16" ),

		/** Png image file: sha224 checksum */

		SHA224_NULL ( "f469e6ef7e8374adecb227e9553c94cbe4e54c" + 
				"57fec902b195ff339e" ),

		/** Png image file: sha256 checksum */

		SHA256_NULL ( "3a756e3f228ffdc419782a6d871e5264575c51" + 
				"f722aa36431cdf74890ce2570b" ),

		/** Png image file: sha512 checksum */

		SHA512_NULL ( "4028505de7e277069fddd67e12e9f088758845" + 
				"e26dd217b6845582dcfb0291faaa4f2ed74af4f83b81" + 
				"cdbdcbd0a237f6113bb12ad7c4a4b92b2d8c42340022" + 
				"ba" ),

		/** Png image file: known checksums */
				
		TYPES ( "md5","sha1","sha224","sha256","sha512" );
		
		private String _checksumStr;
		
		private String[] _types;
		
		
		pngImageChecksums(String checksumIn)
		{
			set_checksumStr(checksumIn);
			
		}//constructor
		
		
		pngImageChecksums(String... typesIn)
		{
			set_types(typesIn);
			
		}//constructor

		
		/* encap methods */
		
		private void set_types(String[] typesIn)
		{
			_types = typesIn;
			
		}//method
		

		private String[] get_types()
		{
			return _types;
			
		}//method

		
		private void set_checksumStr(String checksumIn)
		{
			_checksumStr = checksumIn;
			
		}//method

		
		private String get_checksumStr()
		{
			return _checksumStr;
			
		}//method

		
		/* functional methods */
		
		protected String val()
		{
			return get_checksumStr();
			
		}//method
		

		protected String[] supported()
		{
			return get_types();
			
		}//method
		
		/**
		 * Convert String to Enum
		 *  
		 * @return Enum pngImageChecksums
		 * 
		 * @throws Exception if String not match known Enum type
		 */
		
		protected pngImageChecksums fromStr(String typeInStr) 
				throws Exception
		{
			switch (typeInStr)
			{
				case "md5":
					return pngImageChecksums.MD5_NULL;

				case "sha1":
					return pngImageChecksums.SHA1_NULL;

				case "sha224":
					return pngImageChecksums.SHA224_NULL;

				case "sha256":
					return pngImageChecksums.SHA256_NULL;

				case "sha512":
					return pngImageChecksums.SHA512_NULL;
					
				default:
					throw new Exception("Unrecognized pngImageChecksum " + 
							"type: " + typeInStr);
			}//switch
			
		}//method
	
	 }//enum	

	
	
	static enum charChecksums
	{		
		/** Single Character file: reserved Char of empty */
		
		EMPTY (0), 

		/** Single Character file: reserved Char of null */
		
		NULL (1);		
		
		private final Map<String,String> _checksums = 
				
				new HashMap<String,String>();
		
		/** Single Character file: checksum types */

		private final String[] _hashTypes = { "MD5","SHA1","SHA224","SHA256",
				"SHA512" };

		/** Single Character file: reserved Char of empty - checksum values */
		
		private final String[] _emptyCharHashes =
				
			{   "da564f38413a243e30e8c8c07fccc5d8", 
				
				"b54664965911c6fe91e18cd01b68a75c8183b530",
				
				"52e51677185f8276e34f361801824081e962c7d300fddd6e1e0083f6",
				
				"3e151409ace91cb3394fecd59e92b5dc42c0aad29993a1858f2f70a0" + 
				"866a539b",
				
				"552c8978b796d63c4fc503b654fe3d8ce21fe00cc7b8a406502f3e3a" + 
				"912fc17aa7e31044a04811000ab55b151338091472e7a32d95d5a2bc" +
				"8759fb0c1967782e"
			};

		/** Single Character file: reserved Char of null - checksum values */
		
		private final String[] _nullCharHashes = 
				
			{   "93b885adfe0da089cdf634904fd59f71", 
				
				"5ba93c9db0cff93f52b521d7420e43f6eda2784f",
				
				"fff9292b4201617bdc4d3053fce02734166a683d7d858a7f5f59b073",
				
				"6e340b9cffb37a989ca544e6bb780a2c78901d3fb33738768511a306" + 
				"17afa01d", 
				
				"b8244d028981d693af7b456af8efa4cad63d282e19ff14942c246e50" +
				"d9351d22704a802a71c3580b6370de4ceb293c324a8423342557d4e5" +
				"c38438f0e36910ee"
			};

		/** 
		 * Constructor
		 * 
		 *  @param charNm Integer flag representing file: 0 empty or 1 null 
		 *  Character
		 */
		
		charChecksums(Integer charNm)
		{
			get_checksums().clear();
			
			for(Integer v=0; v < get_hashTypes().length; v++)
			{
				String checksumStr = charNm > 0 ? get_nullCharHashes()[v] : 
					
					get_emptyCharHashes()[v];
				
				get_checksums().put( get_hashTypes()[v], checksumStr );
				
			}//for
			
		}//constructor
		
		
		/* encap methods */

		
		private Map<String,String> get_checksums(){ return _checksums; }

		
		private String[] get_hashTypes()
		{
			return _hashTypes;
			
		}//method
		
		
		private String[] get_emptyCharHashes()
		{
			return _emptyCharHashes;
			
		}//method

		
		private String[] get_nullCharHashes()
		{
			return _nullCharHashes;
			
		}//method
		
		
		/* functional methods */
		
		/**
		 * hashData: Map [hash algorithm] = [checksum value expected] 
		 * 
		 * @return Map [String,String] checksums hash algorithm = checksum 
		 */
		
		protected Map<String,String> hashData()
		{ 
			return Collections.unmodifiableMap( get_checksums() ); 
			
		}//method
		
	}//enum
	
}//interface
