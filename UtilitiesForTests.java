package sse659.project2;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.logging.Logger;

import jonelo.jacksum.JacksumAPI;

/**
 * Helper program
 * 
 * <ul>
 * 
 * <li>create files containing single reserved Character for JUnit 
 * testing
 * 
 * <li>create file with battery of test results for every hash algorithm
 * 
 * </ul>
 * 
 * @see sse659.project2.Config
 * 
 * @author Hatfield, Kevin
 */

public class UtilitiesForTests implements Config
{
	/** 
	 * Output record number, log of data captured from Jacksum methods for 
	 * use in JUnit tests 
	 */
	
	private static Integer _logRecKey = 0;
	
	UtilitiesForTests()
	{
		super();
		
	}//constructor
	
	
	/* encap methods */
	
	
	private static void set_logRecKey(Integer recNum){	_logRecKey = recNum; }
	
	
	private static Integer get_logRecKey(){ return _logRecKey; }
	
	
	/* main */
	
	
	public static void main(String args[]) throws Exception
	{
		UtilitiesForTests utilTests = new UtilitiesForTests();
		utilTests.spawnTestFiles();
		
	}//main
	
	
	/* functional methods */

	
	private static void incrLogKey()
	{
		set_logRecKey( get_logRecKey() + 1 );
		
	}//method

	
	/**
	 * Retrieve test file paths and file names in ordered list
	 * 
	 * @return ArrayList<String> test file names
	 * 
	 * @see Config
	 */
	
	protected static ArrayList<String> get_testFiles()
	{
		ArrayList<String> testFiles = new ArrayList<String>();

		testFiles.add(Config.testFiles.EMPTY.val());

		testFiles.add(Config.testFiles.NULL_CHAR_FILE.val());

		testFiles.add(Config.testFiles.EMPTY_CHAR_FILE.val());

		testFiles.add(Config.testFiles.PNG_IMAGE.val());
		
		return testFiles;
		
	}//method
	
	
	/**
	 * <ul>
	 * <li>Create special single Character files
	 * <li>Create file of Jacksum results, battery of tests for every algorithm
	 * </ul> 
	 * @throws Exception 
	 * 
	 * @see Config
	 */
	
	protected void spawnTestFiles() throws Exception
	{
		try
		{
			spawnOneFile( Config.NULL_CHAR , Config.testFiles.
					NULL_CHAR_FILE.val() );
			
			spawnOneFile( Config.EMPTY_CHAR, Config.testFiles.
					EMPTY_CHAR_FILE.val() );
			
			createBatteryOfResults( Config.testFiles.RESULTS_BATTERY.val() );
			
			createCombinedAlgorithmBatteryOfResults( Config.testFiles.
					RESULTS_COMBINED_BATTERY.val() );
		
		}
		catch(IOException errWithFile)
		{
			System.out.print("Warning: error newly minted test file: " + 
					errWithFile.getMessage());
		}
		catch(Exception unexpErr)
		{
			System.out.print("Fail: error creating test file or file of " + 
					"results: " + unexpErr.getMessage());

			throw new Exception(unexpErr);
		}//try

	}//method
	
	
	/**
	 * Create file containing only one Character
	 *   
	 * @param charIn Character write this to file
	 * @param fileNameIn String path and filename for file write
	 * @throws IOException fail to close the newly minted file
	 */
	
	private static void spawnOneFile(Character charIn, String fileNameIn) 
			throws IOException
	{
		FileOutputStream specialCharFileOut = null;
		
		try
		{		
			specialCharFileOut = new FileOutputStream( fileNameIn );
		
			specialCharFileOut.write( charIn );
			
		}
		catch(IOException errFileOut)
		{
			System.out.print( "Error during creation test char file: " 
					+ fileNameIn + " " + errFileOut.getMessage() );			

		}
		finally
		{
			if( specialCharFileOut != null ) specialCharFileOut.close();
			
		}//try

	}//method
	
	
	/**
	 * Comma delmited file containing test file used, algorithm, checksum
	 * 
	 * @param fileName String path and filename for battery of test results
	 * @throws IOException
	 */
	
	protected static void createBatteryOfResults(String fileName) 
			throws IOException
	{
		FileOutputStream csvOut = null;
		
		try
		{
			//ensure fileName available
			csvOut = new FileOutputStream(fileName);

			//gather testfiles

			final ArrayList<String> testFiles = get_testFiles();


			//iterate through all algorithms supported

			@SuppressWarnings("unchecked")
			Map<String,String> algorithms = JacksumAPI.
			getAvailableAlgorithms();

			final ArrayList<String> algorithmsOrdered = 
					new ArrayList<String>();

			final Iterator<String> algorithmsIterator = 
					Collections.unmodifiableMap(algorithms).keySet().
					iterator();

			while( algorithmsIterator.hasNext() )
			{
				algorithmsOrdered.add(algorithmsIterator.next());

			}//while

			Collections.sort(algorithmsOrdered);

			Iterator<String> testFilesIterator = Collections.
					unmodifiableList(testFiles).iterator();


			String testFile = "";

			final Iterator<String> algorithmsAlphaIterator = 
					Collections.unmodifiableList(algorithmsOrdered).
					iterator();

			String algorithm = "";

			final String[][] results = new String[ algorithmsOrdered.size() * 
			                                 testFiles.size() ][3];
			Integer ct=-1;

			while( algorithmsAlphaIterator.hasNext() )
			{
				algorithm = algorithmsAlphaIterator.next();

				while( testFilesIterator.hasNext() )
				{
					testFile = testFilesIterator.next();

					String[] resultsDetails = {testFile , algorithm ,  
							runJacksum(testFile,algorithm) };

					results[++ct] = resultsDetails;

				}//while

				testFilesIterator = testFiles.listIterator(0);

			}//while


			for(int v=0; v<results.length; v++)
			{
				String leadingZeros = "";

				Integer leadingZerosNeeded = Integer.toString(results.length).
						length();

				for(Integer x=0; x < leadingZerosNeeded; x++ )
				{
					leadingZeros += "0";

				}//for

				String lineNum = leadingZeros + (v+1);

				lineNum = lineNum.substring(lineNum.length()-Integer.toString(
						results.length).length());

				String recOut = lineNum + "," +
						results[v][2] + "," + 
						results[v][1] + "," + 
						results[v][0].

						replace(Config.PATH_TESTFILES+
								System.getProperty("file.separator")
								, "") + "\n";
				
				csvOut.write( recOut.getBytes() );
				
			}//for

		}
		catch(IOException fileIOerr)
		{
			throw new IOException( "Fail create battery file test results: " + 
					fileIOerr.getMessage() );
		}
		finally
		{
			if( csvOut != null ) csvOut.close();
			
		}//try
		
	}//method


	/**
	 * Gather Jacksum results for combined algorithms: sha256+whirlpool2<br>
	 * 
	 * Create file containing Jacksum arguments and results
	 *  
	 * @param fileForResults String filename and path for output of combined
	 * algorithms 
	 * 
	 * @throws IOException Fail write output file of combined algorithm tests, 
	 * results
	 * @throws Exception Looping for random, or fail randomly pick algorithms
	 * 
	 * @see JacksumAPI
	 * @see Config
	 */

	private void createCombinedAlgorithmBatteryOfResults(String fileForResults) 
			throws IOException,	Exception
	{
		ArrayList<String> randomAlgorithmsArgs = new ArrayList<String>();
		
		while(randomAlgorithmsArgs.size() < Config.COMBINED_ALGORITHM_TESTS)
		{
			String algorithmsPicked = randomlySelectedAlgorithms();
			
			if( randomAlgorithmsArgs.indexOf(algorithmsPicked) < 0 )
				randomAlgorithmsArgs.add( algorithmsPicked );
		}//for
		
		ListIterator<String> testFilesIter = UtilitiesForTests.
				get_testFiles().listIterator();

		FileOutputStream combinedResultsFile = null;
		
		final Iterator<String> algorithmArgumentsIter = randomAlgorithmsArgs.
				iterator();
		
		try
		{
			combinedResultsFile = new FileOutputStream(fileForResults);

			Integer lineCt = 0;

			final Integer lineTotal = Config.COMBINED_ALGORITHM_TESTS * 
					UtilitiesForTests.get_testFiles().size();

			while( algorithmArgumentsIter.hasNext() )
			{
				String algorithmArg = algorithmArgumentsIter.next(); 
				
				while( testFilesIter.hasNext() )
				{
					String testFile = testFilesIter.next();
					
					testFile = testFile.replace(Config.PATH_TESTFILES+
							System.getProperty("file.separator"), "");
					
					lineCt++;
					
					String leadingZeros = "";
					
					Integer leadingZerosNeeded = Integer.toString(lineTotal)
							.length();

					for(Integer x=0; x < leadingZerosNeeded; x++ )
					{
						leadingZeros += "0";

					}//for

					String lineNum = leadingZeros + (lineCt);
					
					lineNum = lineNum.substring(lineNum.length()-
							Integer.toString(lineTotal).length());

					String detailLine = lineNum + "," + algorithmArg + "," + 
							testFile + "," + UtilitiesForTests.
							runJacksum(testFile, algorithmArg) + "\n";
					
					combinedResultsFile.write( detailLine.getBytes() );

				}//while
				
				testFilesIter = UtilitiesForTests.get_testFiles().
						listIterator(0);
				
			}//while

		}
		catch(IOException fileErr)
		{
			throw new IOException("Unable write "+ fileForResults + 
					" combined results file: " + fileErr.getMessage());
		}
		finally
		{
			if( combinedResultsFile != null ) combinedResultsFile.close();
			
		}//try
		
	}//method
	
	
	/**
	 * Randomly select algorithms from Jacksum list of supported
	 * 
	 * <br><br>'-a md5+sha1' argument for Jacksum CLI 
	 * 
	 * @return Algorithms selected at random as argument: sha1+md5+sha256
	 * 
	 * @throws Exception Looping for random numbers, fail create the Jacksum 
	 * combined algorithm argumnent
	 */
	
	private String randomlySelectedAlgorithms() throws Exception
	{
		@SuppressWarnings("unchecked")
		final Set<String> algorithms = JacksumAPI.
		getAvailableAlgorithms().keySet();

		//get random number of random numbers

		final ArrayList<Integer> rndmNumbers = new ArrayList<Integer>();

		final Integer numberAlgorithmsUsed = new Random().nextInt( 
				(int) Math.ceil(algorithms.size() / 10) ) + 2;

		Integer safety = 0;

		while( rndmNumbers.size() <= numberAlgorithmsUsed )
		{
			Integer rndm = new Random().nextInt( algorithms.size() );

			if( rndmNumbers.indexOf(rndm) == -1 )
			{
				rndmNumbers.add(rndm);

			}//if

			if(safety++ > 1000) throw new Exception("Unable gather random " + 
					"numbers for selection of test algorithms");
		}//while

		if( rndmNumbers.size() < 2 ) throw new Exception("Fail create random" +
				" numbers for algorithms selection");
		
		//use the randoms to select algorithms to test

		final Iterator<String> algorIter = algorithms.iterator();

		Integer algorIndex = 0;

		final ArrayList<String> algorithmsChosen = new ArrayList<String>();

		while( algorIter.hasNext() )
		{
			algorIndex++;

			String algorIn = algorIter.next();

			if( rndmNumbers.indexOf(algorIndex) > -1 )
			{
				algorithmsChosen.add(algorIn);

				if( algorithmsChosen.size() == rndmNumbers.size() ) break;

			}//if

		}//while

		//create combined algorithms parameter arguments for tests of Jacksum

		final ListIterator<String> algorithmsChosenIter = algorithmsChosen.
				listIterator(0); 

		String algorParm = "";

		while( algorithmsChosenIter.hasNext() )
		{
			String algorCurr = algorithmsChosenIter.next();

			algorParm += algorCurr + "+";

		}//while

		algorParm = algorParm.replaceFirst("\\+$", "");

		if( algorParm.length() < 2 || !algorParm.contains("+") ) 
			throw new Exception("Fail create combined algorithms argument: " + 
					algorParm); 

		return algorParm;
		
	}//method
	
	
	/**
	 * Retrieve file contents as single Character instead of byte array
	 * 
	 * @param fileNameIn test file and path: file contains single Character
	 * @return Character file contents as one Character
	 * 
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws Exception
	 */
	
	protected static Character getSingleCharFileContents(String fileNameIn) 
			throws FileNotFoundException, IOException, Exception
	{
		BufferedReader buffRdr = new BufferedReader(
				new FileReader(fileNameIn));
		
		CharBuffer charBuff = CharBuffer.allocate(1);
		
		buffRdr.read(charBuff);
		
		buffRdr.close();

		return charBuff.get(0);		//autobox convert up to Character
		
	}//method
	
	
	/**
	 * exec Jacksum from command line
	 *  
	 * @param fileName String filename for checksum, path optional
	 * @param algorithm String 'sha1' or multiple 'md5+sha1'
	 * @return String Jacksum results from stdOut
	 */
	
	protected static String runJacksum(String fileName, String algorithm)
	{
		//add path if absent
		
		if( !fileName.contains(Config.PATH_TESTFILES) )
		{
			fileName = Config.PATH_TESTFILES + 
					System.getProperty("file.separator") + fileName;
		}//if
		
		PrintStream stdOut = System.out;	//retain where std out directed
		
		String args[] = {"-a", algorithm, "-f",	fileName };

		final ByteArrayOutputStream outputStream = 
				new ByteArrayOutputStream();
		
		System.setOut(new PrintStream(outputStream));

		jonelo.jacksum.cli.Jacksum.main(args);

		System.out.close();
		
		System.setOut(stdOut);	//restore console or logger

		return outputStream.toString().split("\\s")[0];
		
	}//method
	
	
	/** 
	 * Record method byte input, output
	 * 
	 * @param logRaw byte[] Method input data to record in file
	 * @param startRecord Boolean Mark as new record, as input data
	 */
	
	public static void logBytes(byte[] logRaw, Boolean startRecord)
	{
		FileOutputStream logFile = null;
		
		try
		{
			Boolean newLog = get_logRecKey() > 0 ? true : false;
			
			logFile = new FileOutputStream(Config.testFiles.
					LOG_BB_ENCODE_BYTES.val(), newLog);
			
			if( !startRecord )
			{
				logFile.write( ("<output-" + get_logRecKey() + 
						">").getBytes() );

				logFile.write( logRaw );
				
				logFile.write( ("</output-" + get_logRecKey() + 
						">").getBytes() );
				
			}
			else
			{
				incrLogKey();
				
				logFile.write( ("<input-" + get_logRecKey() + 
						">").getBytes() );

				logFile.write( logRaw );
				
				logFile.write( ("</input-" + get_logRecKey() + 
						">").getBytes() );
				
			}//if
			
		}
		catch(IOException fileErr)
		{
			Logger.getGlobal().severe("Unable log bytes to file: " + 
					fileErr.getMessage());
			
		}
		finally
		{
			
			try
			{
				if(logFile != null) logFile.close();
				
			}
			catch(IOException fileErr)
			{
				Logger.getGlobal().severe("Failure closing log bytes file: " + 
						fileErr.getMessage());
				
			}//try
			
		}//try
		
	}//method

	
	public static void logBytes(String logStr, Boolean startRecord)
	{
		logBytes(logStr.getBytes(), startRecord);
		
	}//method
	
}//class
