package sse659.project2;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Test classes, Unit level testing of select JackSum methods
 *
 * @see <a href='http://www.jonelo.de/java/jacksum'>
 * http://www.jonelo.de/java/jacksum</>
 * 
 * @see <a href='http://www.gnupg.org/download/integrity_check.en.html'>
 * http://www.gnupg.org/download/integrity_check.en.html</>
 * 
 * @author Hatfield, Kevin
 */

@RunWith(Suite.class)
@SuiteClasses(
 {
	 TestBubbleBabble.class

 }
)

public class SuiteOfUnitTests{}
